import json
import os
import requests
import shutil
import collections
import operator
import simplejson
from pprint import pprint as pp

ignore_emoji = ['bowtie', 'neckbeard', 'metal', 'glitch_crab', 'dusty_stick',
                'goberserk', 'godmode', 'finnadie', 'feelsgood', 'rage1', 'rage2', 'rage3', 'rage4',
                'beard-skin-tone-1', 'beard-skin-tone-2', 'beard-skin-tone-3', 'beard-skin-tone-4', 'beard-skin-tone-5']

# 1. Populate raw info from https://api.slack.com/methods/emoji.list/test into data.json
# 2. Run: python ./getemoji.py
# 3. Receive bacon. mmmmm bacon.

def dict_raise_on_duplicates(ordered_pairs):
    d = collections.OrderedDict()
    #count=0
    for k, v in ordered_pairs:
        count = 0  # Reset per situation, not per line
        if k in d:
            d[k + '_' + str(count)] = v  # Build new _n name for key
            count += 1
        else:
            d[k]=v
    return d

with open('data.json') as data_file:
    json_data = json.load(data_file, object_pairs_hook=dict_raise_on_duplicates)

#print json.dumps(json_data, indent=4)

#TODO: Add if file exists

for key, url in sorted(json_data["emoji"].items()):  # Sort!
    if not key in ignore_emoji:
        if not url.startswith('alias'):              # Skip aliases
            #print key, url
            response = requests.get(url, stream=True)

            image_ext = os.path.splitext(url)[1]
            output_filename = key + image_ext
            print "-> {}".format(output_filename)

            with open(output_filename, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
